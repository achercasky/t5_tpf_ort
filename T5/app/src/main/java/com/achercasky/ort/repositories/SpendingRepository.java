package com.achercasky.ort.repositories;

import com.achercasky.ort.models.Spending;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by achercasky on 23/09/2017.
 */

public class SpendingRepository {

    private static SpendingRepository instance;

    private List<Spending> spendingList;

    public static SpendingRepository getInstance() {
        if(instance == null) {
            instance = new SpendingRepository();
        }
        return instance;
    }

    public void saveSpendingList(List<Spending> spendingList) {
        this.spendingList = spendingList;
    }

    public List<Spending> getSpendingList() {
        return spendingList;
    }
}
