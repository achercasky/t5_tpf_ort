package com.achercasky.ort.utils;

import android.content.Context;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by achercasky on 07/09/2017.
 */

public class JsonUtils {

    public static String readJsonFromAssets(Context context, String fileName) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
