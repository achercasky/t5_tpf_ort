package com.achercasky.ort.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.achercasky.ort.R;
import com.achercasky.ort.repositories.ReportRepository;
import com.achercasky.ort.ui.fragments.ReportsFragment;
import com.achercasky.ort.utils.Utils;

/**
 * Created by achercasky on 23/09/2017.
 */

public class ReportsActivity extends BaseActivity {

    public static void startActivity(Context context) {
        Intent starter = new Intent(context, ReportsActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setToolBarTitle(getString(R.string.report_title));
    }

    @Override
    protected void setInitialFragment() {
        changeFragment(ReportsFragment.newInstance(), false);
    }

    @Override
    public boolean isSplashScreen() {
        return false;
    }

    @Override
    public void bindView() {

    }

    @Override
    public void injectClass() {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_generate:
                Utils.createFile(this, ReportRepository.getInstance().getTitle(), ReportRepository.getInstance().getReportList());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
