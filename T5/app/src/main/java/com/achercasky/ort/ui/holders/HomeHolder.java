package com.achercasky.ort.ui.holders;

import android.view.View;

import com.achercasky.ort.models.Spending;
import com.achercasky.ort.ui.views.HomeItemView;

/**
 * Created by achercasky on 07/09/2017.
 */

public class HomeHolder extends BaseViewHolder {

    private HomeItemView view;

    public HomeHolder(View itemView) {
        super(itemView);

        this.view = (HomeItemView) itemView;
    }

    @Override
    public void setViewData(Object object) {
        view.loadData((Spending)object);
    }
}
