package com.achercasky.ort.utils;

import android.graphics.Canvas;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

import com.achercasky.ort.ui.holders.CategoryHolder;

/**
 * Created by achercasky on 19/10/17.
 */

public class RecyclerItemDeleteHelper extends ItemTouchHelper.SimpleCallback {

    private Callback callback;

    public RecyclerItemDeleteHelper(Callback callback) {
        super(0, ItemTouchHelper.LEFT);

        this.callback = callback;
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        return false;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        callback.onSwiped(viewHolder, direction, viewHolder.getAdapterPosition());
    }

    //Muestra la vista oculta
    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState,
                            boolean isCurrentlyActive) {
        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
            //HERE IS THE TRICK
            ((CategoryHolder) viewHolder).getContainerFirstView().setTranslationX(dX);

        } else {
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY,
                    actionState, isCurrentlyActive);
        }
    }

    //Resetea la vista a su estado original
    @Override
    public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        getDefaultUIUtil().clearView(((CategoryHolder) viewHolder).getContainerFirstView());
    }


    public interface Callback {
        void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position);
    }
}
