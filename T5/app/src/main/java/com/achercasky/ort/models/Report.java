package com.achercasky.ort.models;

/**
 * Created by achercasky on 26/09/2017.
 */

public class Report {

    private String name;

    private float total;

    public Report(String name, float total) {
        this.name = name;
        this.total = total;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }
}
