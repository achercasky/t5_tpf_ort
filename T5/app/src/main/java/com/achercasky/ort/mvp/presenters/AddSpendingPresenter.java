package com.achercasky.ort.mvp.presenters;

import com.achercasky.ort.models.Spending;
import com.achercasky.ort.mvp.views.AddSpendingMvpView;
import com.chachapps.initialclasses.mvp.presenter.BasePresenter;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by achercasky on 09/09/2017.
 */

public class AddSpendingPresenter extends BasePresenter<AddSpendingMvpView> {

    public void validateInputs(String category, String amount, String description) {

        if(category.isEmpty()) {
            getMvpView().onEmptyCategory();
            return;
        }

        if(amount.equals("")) {
            getMvpView().onEmptyAmount();
            return;
        }

        if(amount.matches("[0]+")) {
            getMvpView().onInvalidAmount();
            return;
        }

        if(description.isEmpty()) {
            getMvpView().onEmptyDescription();
            return;
        }

        Spending spending = new Spending();
        spending.setCategory(category);
        spending.setMonto(Double.valueOf(amount));
        spending.setDescripcion(description);

        getMvpView().onSuccess(spending);
    }
}
