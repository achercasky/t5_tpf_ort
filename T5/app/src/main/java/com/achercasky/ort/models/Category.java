package com.achercasky.ort.models;

import com.squareup.moshi.Json;

/**
 * Created by achercasky on 09/09/2017.
 */

public class Category {

    @Json(name = "id")
    private int id;

    @Json(name = "descripcion")
    private String description;

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
