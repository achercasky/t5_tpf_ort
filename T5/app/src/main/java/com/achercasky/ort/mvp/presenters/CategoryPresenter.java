package com.achercasky.ort.mvp.presenters;

import com.achercasky.ort.models.Category;
import com.achercasky.ort.models.Spending;
import com.achercasky.ort.mvp.views.CategoryMvpView;
import com.achercasky.ort.repositories.CategoryRepository;
import com.achercasky.ort.repositories.SpendingRepository;
import com.chachapps.initialclasses.mvp.presenter.BasePresenter;

import java.util.Random;

/**
 * Created by achercasky on 21/10/2017.
 */

public class CategoryPresenter extends BasePresenter<CategoryMvpView> {

    public void existCategory(String title) {

        if(isCategoryExit(title)) {
            getMvpView().onExistedCategory();
            return;
        }

        getMvpView().onNewCategory(createCategory(title));
    }

    public void renameCategory(Category category, String description) {

        if(isCategoryUse(category.getDescription())) {
            getMvpView().onUsingCategory();
            return;
        }


        if(isCategoryExit(description)) {
            getMvpView().onExistedCategory();
            return;
        }

        category.setDescription(description);
        getMvpView().onEditCategory(category);
    }

    public void removeCategory(Category category, int index) {
        if(isCategoryUse(category.getDescription())) {
            getMvpView().onNotDeletedCategory(category, index);
            return;
        }

        getMvpView().onDeleteCategory(category, index);

    }

    private Category createCategory(String title){
        final Random random = new Random();

        Category category = new Category();
        category.setDescription(title);
        category.setId(random.nextInt());

        return category;
    }

    private boolean isCategoryExit(String text) {
        boolean exist = false;
        for (Category category : CategoryRepository.getInstance().getCategoryList()) {
            if (category.getDescription().equals(text)) {
                exist = true;
            }
        }
        return exist;
    }

    private boolean isCategoryUse(String category) {
        boolean isUsing = false;
        for(Spending spending: SpendingRepository.getInstance().getSpendingList()) {
            if(spending.getCategory().equals(category)) {
                isUsing = true;
            }
        }
        return isUsing;
    }
}
