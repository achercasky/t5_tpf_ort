package com.achercasky.ort.repositories;

import com.achercasky.ort.models.Category;

import java.util.List;

/**
 * Created by achercasky on 26/09/2017.
 */

public class CategoryRepository {

    private static CategoryRepository instance;

    private List<String> categories;

    private List<Category> categoryList;

    public static CategoryRepository getInstance() {
        if(instance == null) {
            instance = new CategoryRepository();
        }
        return instance;
    }

    public void saveCategories(List<String> categories) {
        this.categories = categories;
    }

    public List<String> getCategories() {
        return categories;
    }

    public List<Category> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }
}
