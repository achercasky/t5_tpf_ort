package com.achercasky.ort.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.os.Environment;
import android.text.format.DateFormat;
import android.widget.Toast;

import com.achercasky.ort.R;
import com.achercasky.ort.models.Report;

/**
 * Created by achercasky on 26/09/2017.
 */

public class Utils {

    public static String getMonthName() {
        return (String) DateFormat.format("MMMM", new Date());
    }

    public static String getCurrentYear() {
        return String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
    }

    public static String capitalizeWord(String myString) {
        return myString.substring(0,1).toUpperCase() + myString.substring(1);
    }

    public static void createFile(Context context, String sFileName, List<Report> sBody) {
        try {
            File root = new File(Environment.getExternalStorageDirectory(), context.getString(R.string.title));
            if (!root.exists()) {
                root.mkdirs();
            }
            File gpxfile = new File(root, sFileName);
            FileWriter writer = new FileWriter(gpxfile);

            for(Report report: sBody) {
                writer.append(report.getName() + " " + report.getTotal());
                writer.append("\n");
            }

            writer.flush();
            writer.close();
            Toast.makeText(context, context.getString(R.string.saved), Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
