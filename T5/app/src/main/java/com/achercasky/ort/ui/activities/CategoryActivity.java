package com.achercasky.ort.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.achercasky.ort.R;
import com.achercasky.ort.ui.fragments.CategoryFragment;

/**
 * Created by achercasky on 19/10/17.
 */

public class CategoryActivity extends BaseActivity {

    public static void startActivity(Context context) {
        Intent starter = new Intent(context, CategoryActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setToolBarTitle(getString(R.string.title_category));
    }

    @Override
    protected void setInitialFragment() {
        changeFragment(CategoryFragment.newInstance(), false);
    }

    @Override
    public boolean isSplashScreen() {
        return false;
    }

    @Override
    public void bindView() {

    }

    @Override
    public void injectClass() {

    }
}
