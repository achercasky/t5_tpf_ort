package com.achercasky.ort.ui.fragments.dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.achercasky.ort.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by achercasky on 20/10/17.
 */

public class CustomDialogFragment extends DialogFragment {

    @BindView(R.id.accept) Button accept;
    @BindView(R.id.cancel) Button cancel;
    @BindView(R.id.dialog_edit) EditText edit;
    @BindView(R.id.dialog_title) TextView titleDialog;

    private Callback callback;

    private static final String KEY = "title";
    private String title;

    private int id;

    public CustomDialogFragment() {}

    public static CustomDialogFragment newInstance() {

        Bundle args = new Bundle();

        CustomDialogFragment fragment = new CustomDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static CustomDialogFragment newInstance(String title) {

        Bundle args = new Bundle();

        CustomDialogFragment fragment = new CustomDialogFragment();
        args.putString(KEY, title);
        fragment.setArguments(args);
        return fragment;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_custom_dialog, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        titleDialog.setText(title);

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String text = edit.getText().toString();
                if(text.length() > 0) {
                    callback.onAccepted(id, text);
                    dismiss();
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        String text = getArguments().getString(KEY);

        if(text != null) {
            this.edit.setHint(text);
        }
    }

    public interface Callback {
        void onAccepted(int id, String name);
    }

}
