package com.achercasky.ort.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.achercasky.ort.R;
import com.achercasky.ort.ui.fragments.AddSpendingFragment;

/**
 * Created by achercasky on 09/09/2017.
 */

public class AddSpendingActivity extends BaseActivity {

    public static void start(Context context) {
        Intent starter = new Intent(context, AddSpendingActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setToolBarTitle(getString(R.string.title_spending));
    }

    @Override
    protected void setInitialFragment() {
        changeFragment(AddSpendingFragment.newInstance(), false);
    }

    @Override
    public boolean isSplashScreen() {
        return false;
    }

    @Override
    public void bindView() {

    }

    @Override
    public void injectClass() {

    }
}
