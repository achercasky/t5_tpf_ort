package com.achercasky.ort.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import com.achercasky.ort.R;
import com.achercasky.ort.db.DbSqliteManager;
import com.achercasky.ort.models.Category;
import com.achercasky.ort.mvp.presenters.CategoryPresenter;
import com.achercasky.ort.mvp.views.CategoryMvpView;
import com.achercasky.ort.repositories.CategoryRepository;
import com.achercasky.ort.ui.adapters.CategoryAdapter;
import com.achercasky.ort.ui.fragments.dialogs.CustomDialogFragment;
import com.achercasky.ort.ui.holders.CategoryHolder;
import com.achercasky.ort.ui.views.CategoryItemView;
import com.achercasky.ort.utils.RecyclerItemDeleteHelper;
import com.achercasky.ort.utils.SnackbarUtils;

import java.util.List;

import butterknife.BindView;

/**
 * Created by achercasky on 19/10/17.
 */

public class CategoryFragment extends BaseFragment implements RecyclerItemDeleteHelper.Callback,
        CustomDialogFragment.Callback, CategoryMvpView, CategoryItemView.Callback, DbSqliteManager.Callback{

    @BindView(R.id.category_recycler) RecyclerView recyclerView;

    @BindView(R.id.category_floating_btn) FloatingActionButton add;

    private CategoryAdapter adapter;

    private CategoryPresenter presenter;

    private static final int CREATE = 0;
    private static final int EDIT = 1;

    private Category category;

    public static CategoryFragment newInstance() {
        
        Bundle args = new Bundle();
        
        CategoryFragment fragment = new CategoryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter = new CategoryPresenter();
        presenter.attachMvpView(this);

        final List<Category> categoryList = DbSqliteManager.getInstance(getActivity()).getCategories();

        adapter = new CategoryAdapter();
        adapter.setCallback(this);
        adapter.setCategoryList(categoryList);
        CategoryRepository.getInstance().setCategoryList(categoryList);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(adapter);

        new ItemTouchHelper(new RecyclerItemDeleteHelper(this)).attachToRecyclerView(recyclerView);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomDialogFragment newFragment = CustomDialogFragment.newInstance();
                newFragment.setCallback(CategoryFragment.this);
                newFragment.setTitle(getString(R.string.category_dialog_create));
                newFragment.setId(CREATE);
                newFragment.show(getFragmentManager(), "");
            }
        });
    }

    @Override
    protected int getMainLayoutResId() {
        return R.layout.fragment_category;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if(viewHolder instanceof CategoryHolder) {

            final List<Category> categories = CategoryRepository.getInstance().getCategoryList();

            //Backup para recuperar el item borrado
            final Category deletedItem = categories.get(viewHolder.getAdapterPosition());
            final int deletedIndex = viewHolder.getAdapterPosition();

            adapter.removeItem(viewHolder.getAdapterPosition());

            presenter.removeCategory(deletedItem, deletedIndex);

        }
    }

    @Override
    public void onAccepted(int id, String name) {

        switch (id) {
            case CREATE:
                presenter.existCategory(name);
                break;
            case EDIT:
                presenter.renameCategory(category, name);
                break;
        }
    }

    @Override
    public void onExistedCategory() {
        SnackbarUtils.show(getView(), getString(R.string.category_already_exist));
    }

    @Override
    public void onNewCategory(Category category) {
        DbSqliteManager.getInstance(getActivity()).writeCategoryOnDB(category, this);
    }

    @Override
    public void onEditCategory(Category category) {
        DbSqliteManager.getInstance(getActivity()).editCategoryOnDB(category, this);
    }

    @Override
    public void onUsingCategory() {
        SnackbarUtils.show(getView(), getString(R.string.category_using));
    }

    @Override
    public void onNotDeletedCategory(Category category, int index) {
        adapter.restoreItem(category, index);
        SnackbarUtils.show(getView(), getString(R.string.category_using));
    }

    @Override
    public void onDeleteCategory(final Category category, final int index) {
        DbSqliteManager.getInstance(getActivity()).removeCategoryOnDB(category, this);

        SnackbarUtils.show(getView(), category.getDescription() + " " + getString(R.string.item_remove),
                getString(R.string.undo),new View.OnClickListener(){
                    @Override
                    public void onClick(View view) {
                        adapter.restoreItem(category, index);

                        onNewCategory(category);
                    }
                });
    }

    @Override
    public Context getMvpContext() {
        return getActivity();
    }

    @Override
    public void onError(Throwable throwable) {

    }

    @Override
    public void onNoInternetConnection() {

    }

    @Override
    public void onErrorCode(String s) {

    }

    @Override
    public void onItemSelected(Category category) {

        this.category = category;

        CustomDialogFragment newFragment = CustomDialogFragment.newInstance(category.getDescription());
        newFragment.setCallback(CategoryFragment.this);
        newFragment.setTitle(getString(R.string.category_dialog_edit));
        newFragment.setId(EDIT);
        newFragment.show(getFragmentManager(), "");
    }

    @Override
    public void onSaveDb() {

        final List<Category> categories = DbSqliteManager.getInstance(getActivity()).getCategories();
        CategoryRepository.getInstance().setCategoryList(categories);

        SnackbarUtils.show(getView(), getString(R.string.category_save));

        adapter.setCategoryList(categories);
        adapter.notifyDataSetChanged();
    }
}
