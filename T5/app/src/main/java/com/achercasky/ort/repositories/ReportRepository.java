package com.achercasky.ort.repositories;

import com.achercasky.ort.models.Report;

import java.util.List;

/**
 * Created by achercasky on 3/10/17.
 */

public class ReportRepository {

    private static ReportRepository instance;

    private String title;

    private List<Report> reportList;

    private int total;

    public static ReportRepository getInstance() {
        if(instance == null) {
            instance = new ReportRepository();
        }
        return instance;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Report> getReportList() {
        return reportList;
    }

    public void setReportList(List<Report> reportList) {
        this.reportList = reportList;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
