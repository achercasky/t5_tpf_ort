package com.achercasky.ort.ui.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.achercasky.ort.R;
import com.chachapps.initialclasses.activity.InitialActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by achercasky on 24/08/2017.
 */

public abstract class BaseActivity extends InitialActivity{

    @BindView(R.id.frame)
    FrameLayout frameLayout;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.toolbar_title) TextView toolbarTitle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        setInitialFragment();
    }

    @Override
    public int getMyFragment() {
        return frameLayout.getId();
    }

    /**
     * This method is used for the BaseFragment class.
     * @param fragment
     */
    public void changeFragment(Fragment fragment, boolean addToBackStack) {
        changeFragment(fragment, addToBackStack, FragmentTransaction.TRANSIT_NONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected void setToolBarTitle(String title) {
        toolbarTitle.setText(title);
    }

    protected abstract void setInitialFragment();
}
