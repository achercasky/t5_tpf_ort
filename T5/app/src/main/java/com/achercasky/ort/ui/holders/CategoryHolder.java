package com.achercasky.ort.ui.holders;

import android.view.View;

import com.achercasky.ort.models.Category;
import com.achercasky.ort.models.Spending;
import com.achercasky.ort.ui.views.CategoryItemView;
import com.achercasky.ort.ui.views.HomeItemView;

/**
 * Created by achercasky on 07/09/2017.
 */

public class CategoryHolder extends BaseViewHolder {

    private CategoryItemView view;

    public CategoryHolder(View itemView) {
        super(itemView);

        this.view = (CategoryItemView) itemView;
    }

    @Override
    public void setViewData(Object object) {
        view.loadData((Category)object);
    }

    public View getContainerFirstView() {
        return view.getContainerFirstView();
    }
}
