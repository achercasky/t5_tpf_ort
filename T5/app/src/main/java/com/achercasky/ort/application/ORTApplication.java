package com.achercasky.ort.application;

import android.app.Application;

import com.facebook.stetho.Stetho;

/**
 * Created by achercasky on 09/09/2017.
 */

public class ORTApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Stetho.initializeWithDefaults(this);
    }
}
