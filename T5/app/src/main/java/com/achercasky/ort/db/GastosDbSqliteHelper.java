package com.achercasky.ort.db;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import com.achercasky.ort.BuildConfig;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by achercasky on 09/09/2017.
 */

public class GastosDbSqliteHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "gastos.sqlite";

    private Context context;

    private String DB_PATH = "/data/data/"+ BuildConfig.APPLICATION_ID +"/databases/";

    private SQLiteDatabase myDataBase;

    public GastosDbSqliteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;

        this.createDataBase();
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    /**
     * Crea una db vacia y la reescribe con mi base.
     * */
    public void createDataBase(){
        try {
            boolean dbExist = checkDataBase();

            if(dbExist){
                //Nada, la db ya existe.
            }else{
                //Llamar a este metodo y una db vacia se creara en system path de la app. Asi se podra sobre escribir.
                this.getReadableDatabase();

                copyDataBase();
            }
        }
        catch (Exception e) {}
    }

    /**
     * Valida si la db existe asi no se re crea cada vez que se abre la app.
     * @return true si existe, false si no
     */
    private boolean checkDataBase(){

        SQLiteDatabase checkDB = null;

        try{
            String myPath = DB_PATH + DATABASE_NAME;
            checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);

        }catch(SQLiteException e){
            //db no existe
        }

        if(checkDB != null){
            checkDB.close();
        }

        return checkDB != null ? true : false;
    }

    /**
     * Copia la db local de los assets-folder a la db vacia de le carpeta del sistema.
     * Esto se hace usando el transfering bytestream.
     * */
    private void copyDataBase(){

        try{
            //Se abre la local db como input stream
            InputStream myInput = context.getAssets().open(DATABASE_NAME);

            // Ruta hacia la db que fue creada vacia.
            String outFileName = DB_PATH + DATABASE_NAME;

            //Se abre la db vacia como output stream
            OutputStream myOutput = new FileOutputStream(outFileName);

            //Transfiere los bytes de inputfile a el outputfile
            byte[] buffer = new byte[1024];
            int length;
            while ((length = myInput.read(buffer))>0){
                myOutput.write(buffer, 0, length);
            }

            //Cierra los streams
            myOutput.flush();
            myOutput.close();
            myInput.close();
        }
        catch (Exception e) {

        }
    }

    public SQLiteDatabase openDataBase() throws SQLException{
        String myPath = DB_PATH + DATABASE_NAME;
        myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
        return myDataBase;
    }

    @Override
    public synchronized void close() {
        if(myDataBase != null) {
            myDataBase.close();
        }

        super.close();
    }

}
