package com.achercasky.ort.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.achercasky.ort.models.Spending;
import com.achercasky.ort.ui.holders.BaseViewHolder;
import com.achercasky.ort.ui.holders.HomeHolder;
import com.achercasky.ort.ui.views.HomeItemView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by achercasky on 07/09/2017.
 */

public class HomeAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private List<Spending> spendingList;

    public HomeAdapter() {
        this.spendingList = new ArrayList<>();
    }

    public void setSpendingList(List<Spending> spendingList) {
        this.spendingList = spendingList;
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new HomeHolder(new HomeItemView(parent.getContext()));
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {

        Spending spending = spendingList.get(position);
        holder.setViewData(spending);
    }

    @Override
    public int getItemCount() {
        return spendingList.size();
    }
}
