package com.achercasky.ort.db;

import android.provider.BaseColumns;

import com.achercasky.ort.utils.Constants;

/**
 * Created by achercasky on 09/09/2017.
 */

public class SpendingEntry implements BaseColumns {

    public static final String TABLE_NAME = "gasto";
    public static final String COLUMN_NAME_CATEGORIA = "categoria";
    public static final String COLUMN_NAME_DESCRIPCION = Constants.DESCRIPCION;
    public static final String COLUMN_NAME_MONTO = "monto";
    public static final String COLUMN_NAME_FECHA = "fecha";
}
