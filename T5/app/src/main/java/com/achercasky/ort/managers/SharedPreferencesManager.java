package com.achercasky.ort.managers;

import android.content.Context;
import android.content.SharedPreferences;

import com.achercasky.ort.R;
import com.achercasky.ort.utils.Constants;

/**
 * Created by achercasky on 09/09/2017.
 */

public class SharedPreferencesManager {

    private static SharedPreferencesManager instance;

    private SharedPreferences sharedPreferences;

    public static SharedPreferencesManager getInstance(Context context) {
        if(instance == null) {
            instance = new SharedPreferencesManager(context);
        }
        return instance;
    }

    private SharedPreferencesManager(Context context) {
        sharedPreferences = context.getSharedPreferences(context.getString(R.string.preference_file_key),
                Context.MODE_PRIVATE);
    }

    public void writeBoolean(boolean value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(Constants.SP_IS_DATA_SAVED, (Boolean) value);
        editor.commit();
    }

    public boolean getBooleanValue(String key) {
        return sharedPreferences.getBoolean(key, false);
    }
}
