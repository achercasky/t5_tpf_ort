package com.achercasky.ort.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.achercasky.ort.models.Category;
import com.achercasky.ort.models.Spending;
import com.achercasky.ort.ui.holders.BaseViewHolder;
import com.achercasky.ort.ui.holders.CategoryHolder;
import com.achercasky.ort.ui.holders.HomeHolder;
import com.achercasky.ort.ui.views.CategoryItemView;
import com.achercasky.ort.ui.views.HomeItemView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by achercasky on 19/10/2017.
 */

public class CategoryAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private List<Category> categoryList;

    private CategoryItemView.Callback callback;

    public CategoryAdapter() {
        this.categoryList = new ArrayList<>();
    }

    public void setCallback(CategoryItemView.Callback callback) {
        this.callback = callback;
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CategoryHolder(new CategoryItemView(parent.getContext(), callback));
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {

        Category category = categoryList.get(position);
        holder.setViewData(category);
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public void removeItem(int position) {
        categoryList.remove(position);

        notifyItemRemoved(position);
    }

    public void restoreItem(Category category, int position) {
        categoryList.add(position, category);

        notifyItemInserted(position);
    }
}
