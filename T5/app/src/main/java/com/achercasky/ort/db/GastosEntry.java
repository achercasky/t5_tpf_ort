package com.achercasky.ort.db;

import android.provider.BaseColumns;

import com.achercasky.ort.utils.Constants;

/**
 * Created by achercasky on 09/09/2017.
 */

public class GastosEntry implements BaseColumns{

    public static final String TABLE_NAME = "categoria";
    public static final String COLUMN_NAME_ID = Constants.ID;
    public static final String COLUMN_NAME_DESCRIPCION = Constants.DESCRIPCION;
}
