package com.achercasky.ort.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.achercasky.ort.R;
import com.achercasky.ort.db.DbSqliteManager;
import com.achercasky.ort.managers.SharedPreferencesManager;
import com.achercasky.ort.models.Category;
import com.achercasky.ort.models.Spending;
import com.achercasky.ort.repositories.SpendingRepository;
import com.achercasky.ort.ui.activities.AddSpendingActivity;
import com.achercasky.ort.ui.activities.ReportsActivity;
import com.achercasky.ort.ui.adapters.HomeAdapter;
import com.achercasky.ort.utils.Constants;
import com.achercasky.ort.utils.JsonUtils;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Types;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by achercasky on 07/09/2017.
 */

public class HomeFragment extends BaseFragment {

    @BindView(R.id.home_recycler) RecyclerView recyclerView;

    @BindView(R.id.home_btn_new) Button newSpending;
    @BindView(R.id.home_btn_report) Button reports;

    @BindView(R.id.home_progress_linear) RelativeLayout linearPorgress;
    @BindView(R.id.container) RelativeLayout container;

    @BindView(R.id.home_empty) TextView emptyTxt;

    private HomeAdapter adapter;

    private boolean isComingBack;

    private List<Spending> spendingList;

    public static HomeFragment newInstance() {
        
        Bundle args = new Bundle();
        
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getMainLayoutResId() {
        return R.layout.fragment_home;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        String json = JsonUtils.readJsonFromAssets(getContext(), Constants.CATEGORIA_JSON);

        Type type = Types.newParameterizedType(List.class, Category.class);
        JsonAdapter<List<Category>> jsonAdapter = getMoshi().adapter(type);
        List<Category> categories = new ArrayList<>();
        try {
            categories = jsonAdapter.fromJson(json);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(!SharedPreferencesManager.getInstance(getActivity()).getBooleanValue(Constants.SP_IS_DATA_SAVED)) {
            DbSqliteManager.getInstance(getContext()).writeOnDB(categories);
            SharedPreferencesManager.getInstance(getActivity()).writeBoolean(true);
            container.setVisibility(View.VISIBLE);
            linearPorgress.setVisibility(View.GONE);
        } else {
            container.setVisibility(View.VISIBLE);
            linearPorgress.setVisibility(View.GONE);
        }

        createAdapter();

        newSpending.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddSpendingActivity.start(getActivity());
            }
        });

        reports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                DbSqliteManager.getInstance(getActivity()).removeAllSpendings();
                ReportsActivity.startActivity(getActivity());
            }
        });
    }

    private void createAdapter() {
        spendingList = DbSqliteManager.getInstance(getActivity()).getSpendings();

        adapter = new HomeAdapter();

        if(spendingList.size() == 0) {
            emptyTxt.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            emptyTxt.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);

            SpendingRepository.getInstance().saveSpendingList(spendingList);

            adapter.setSpendingList(spendingList);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerView.setAdapter(adapter);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if(isComingBack) {
            createAdapter();
            isComingBack = false;
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        isComingBack = true;
    }
}
