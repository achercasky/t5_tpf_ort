package com.achercasky.ort.ui.views;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.achercasky.ort.R;
import com.achercasky.ort.models.Category;
import com.achercasky.ort.models.Spending;
import com.achercasky.ort.utils.DateUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by achercasky on 07/09/2017.
 */

public class CategoryItemView extends LinearLayout {

    @BindView(R.id.item_category_title) TextView title;

    @BindView(R.id.view_background) View viewForeground;

    @BindView(R.id.container) RelativeLayout container;

    private Callback callback;

    public CategoryItemView(Context context) {
        super(context);
        inflateView();
    }

    public CategoryItemView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        inflateView();
    }

    public CategoryItemView(Context context, Callback callback) {
        super(context);

        inflateView();
        this.callback = callback;
    }

    private void inflateView() {
        inflate(getContext(), R.layout.item_category, this);

        ButterKnife.bind(this);
    }

    public void loadData(final Category category) {
        title.setText(category.getDescription());

        container.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.onItemSelected(category);
            }
        });
    }

    public View getContainerFirstView() {
        return container;
    }

    public interface Callback {
        void onItemSelected(Category category);
    }
}
