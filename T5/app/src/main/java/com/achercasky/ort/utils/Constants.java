package com.achercasky.ort.utils;

/**
 * Created by achercasky on 09/09/2017.
 */

public class Constants {

    public static final String ID = "id";
    public static final String DESCRIPCION = "descripcion";

    public static final String SP_IS_DATA_SAVED = "isDataSaved";

    public static final String CATEGORIA_JSON = "categoria.json";
}
