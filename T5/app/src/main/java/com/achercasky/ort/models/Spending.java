package com.achercasky.ort.models;

/**
 * Modelo para los Gastos.
 *
 * Created by achercasky on 07/09/2017.
 */

public class Spending {

    private String category;
    private String descripcion;
    private String fecha;

    private Double monto;

    public Spending() {}

    public Spending(String category, String descripcion, String fecha, Double monto) {
        this.category = category;
        this.descripcion = descripcion;
        this.fecha = fecha;
        this.monto = monto;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Double getMonto() {
        return monto;
    }

    public void setMonto(Double monto) {
        this.monto = monto;
    }
}
