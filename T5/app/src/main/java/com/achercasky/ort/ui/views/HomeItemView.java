package com.achercasky.ort.ui.views;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.achercasky.ort.R;
import com.achercasky.ort.models.Spending;
import com.achercasky.ort.utils.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by achercasky on 07/09/2017.
 */

public class HomeItemView extends LinearLayout {

    @BindView(R.id.item_home_date) TextView date;
    @BindView(R.id.item_home_price) TextView price;
    @BindView(R.id.item_home_category) TextView category;
    @BindView(R.id.item_home_description) TextView description;

    public HomeItemView(Context context) {
        super(context);
        inflateView();
    }

    public HomeItemView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        inflateView();
    }

    private void inflateView() {
        inflate(getContext(), R.layout.item_home, this);

        ButterKnife.bind(this);
    }

    public void loadData(Spending spending) {

        date.setText(DateUtils.parseDateToddMMyyyy(spending.getFecha()));
        category.setText(spending.getCategory());
        description.setText(spending.getDescripcion());

        price.setText("$" + String.valueOf(spending.getMonto()));
    }


}
