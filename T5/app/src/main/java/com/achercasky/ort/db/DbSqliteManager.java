package com.achercasky.ort.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.achercasky.ort.models.Category;
import com.achercasky.ort.models.Report;
import com.achercasky.ort.models.Spending;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by achercasky on 09/09/2017.
 */

public class DbSqliteManager {

    private static DbSqliteManager instance;

    private SQLiteDatabase database;

    private GastosDbSqliteHelper dbSqliteHelper;

    public static synchronized DbSqliteManager getInstance(Context context) {
        if(instance == null) {
            instance = new DbSqliteManager(context);
        }

        return instance;
    }

    public static synchronized DbSqliteManager getInstance() {
        if(instance == null) {
            instance = new DbSqliteManager();
        }

        return instance;
    }

    private DbSqliteManager(Context context) {
        dbSqliteHelper = new GastosDbSqliteHelper(context);
    }

    private DbSqliteManager() {}

    public synchronized void writeOnDB(List<Category> categories) {
        database = dbSqliteHelper.getWritableDatabase();
        database.beginTransaction();

        ContentValues contentValues = new ContentValues();

        try {
            for (Category category : categories) {
                contentValues.put(GastosEntry.COLUMN_NAME_ID, category.getId());
                contentValues.put(GastosEntry.COLUMN_NAME_DESCRIPCION, category.getDescription());
                database.insert(GastosEntry.TABLE_NAME, null, contentValues);
            }
            database.setTransactionSuccessful();
        }
        finally {
            database.endTransaction();
        }

        database.close();

    }

    public synchronized void writeOnDB(Spending spending, Callback callback) {
        database = dbSqliteHelper.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(SpendingEntry.COLUMN_NAME_CATEGORIA, spending.getCategory());
        contentValues.put(SpendingEntry.COLUMN_NAME_MONTO, spending.getMonto());
        contentValues.put(SpendingEntry.COLUMN_NAME_DESCRIPCION, spending.getDescripcion());
        contentValues.put(SpendingEntry.COLUMN_NAME_FECHA, spending.getFecha());
        database.insert(SpendingEntry.TABLE_NAME, null, contentValues);

        database.close();
        database = null;

        callback.onSaveDb();
    }

    public synchronized List<Category> getCategories() {
        database = dbSqliteHelper.getReadableDatabase();

        List<Category> categories = new ArrayList<>();

        //select descripcion, id from categoria group by descripcion

        //Columnas
        String[] projection = {
                GastosEntry.COLUMN_NAME_DESCRIPCION,
                GastosEntry.COLUMN_NAME_ID
        };

        String groupBy = GastosEntry.COLUMN_NAME_DESCRIPCION;

        Cursor c = database.query(
                GastosEntry.TABLE_NAME,
                projection,
                null,
                null,
                groupBy,
                null,
                null

        );

        if(c != null) {
            while(c.moveToNext()){
                final Category category = new Category();
                category.setDescription(c.getString(c.getColumnIndex(GastosEntry.COLUMN_NAME_DESCRIPCION)));
                category.setId(c.getInt(c.getColumnIndex(GastosEntry.COLUMN_NAME_ID)));

                categories.add(category);
            }
        }

        c.close();

        database.close();
        database = null;

        return categories;
    }

    public synchronized List<String> getDescriptionsFromCategory() {
        database = dbSqliteHelper.getReadableDatabase();

        List<String> descriptions = new ArrayList<>();

        //select descripcion from categoria group by descripcion

        //Columnas
        String[] projection = {
                GastosEntry.COLUMN_NAME_DESCRIPCION
        };

        String groupBy = GastosEntry.COLUMN_NAME_DESCRIPCION;

        Cursor c = database.query(
                GastosEntry.TABLE_NAME,
                projection,
                null,
                null,
                groupBy,
                null,
                null

        );

        if(c != null) {
            while(c.moveToNext()){
                String descripcion = c.getString(c.getColumnIndex(GastosEntry.COLUMN_NAME_DESCRIPCION));

                descriptions.add(descripcion);
            }
        }

        c.close();

        database.close();
        database = null;

        return descriptions;
    }

    public synchronized List<Spending> getSpendings() {
        database = dbSqliteHelper.getReadableDatabase();

        List<Spending> spendingList = new ArrayList<>();

        //select Monto, Categoria, Descripcion, Fecha from gasto

        //Columnas
        String[] projection = {
                SpendingEntry.COLUMN_NAME_MONTO,
                SpendingEntry.COLUMN_NAME_CATEGORIA,
                SpendingEntry.COLUMN_NAME_DESCRIPCION,
                SpendingEntry.COLUMN_NAME_FECHA
        };

        String orderBy = SpendingEntry.COLUMN_NAME_FECHA + " " + "DESC";

        Cursor c = database.query(
                SpendingEntry.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                orderBy
        );

        if(c != null) {
            while(c.moveToNext()){
                String descripcion = c.getString(c.getColumnIndex(SpendingEntry.COLUMN_NAME_DESCRIPCION));
                String cate = c.getString(c.getColumnIndex(SpendingEntry.COLUMN_NAME_CATEGORIA));
                String fecha = c.getString(c.getColumnIndex(SpendingEntry.COLUMN_NAME_FECHA));
                Double monto = c.getDouble(c.getColumnIndex(SpendingEntry.COLUMN_NAME_MONTO));

                spendingList.add(new Spending(cate, descripcion, fecha, monto));
            }
        }

        c.close();

        database.close();
        database = null;

        return spendingList;
    }

    public synchronized int getTotalFromCategory(String year, String month, String category) {
        database = dbSqliteHelper.getReadableDatabase();

        int total = 0;

        String[] params = new String[]{month, category, year};

        String query = "SELECT SUM(monto) FROM Gasto WHERE strftime('%m', fecha) = ? AND categoria = ? " +
                "AND strftime('%Y', \n" + "fecha) = ?";

        Cursor c = database.rawQuery(query, params);

        if(c != null) {
            while(c.moveToNext()){
                total = c.getInt(0);
            }
        }

        c.close();

        database.close();
        database = null;

        return total;
    }

    public void removeAllSpendings() {
        database = dbSqliteHelper.getReadableDatabase();

        database.delete(SpendingEntry.TABLE_NAME, null, null);

        database.close();
        database = null;
    }

    public synchronized void writeCategoryOnDB(Category category, Callback callback) {
        database = dbSqliteHelper.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(GastosEntry.COLUMN_NAME_DESCRIPCION, category.getDescription());
        contentValues.put(GastosEntry.COLUMN_NAME_ID, category.getId());

        database.insert(GastosEntry.TABLE_NAME, null, contentValues);

        database.close();
        database = null;

        callback.onSaveDb();
    }

    public synchronized void editCategoryOnDB(Category category, Callback callback) {
        database = dbSqliteHelper.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(GastosEntry.COLUMN_NAME_DESCRIPCION, category.getDescription());
        contentValues.put(GastosEntry.COLUMN_NAME_ID, category.getId());

        database.update(GastosEntry.TABLE_NAME, contentValues, GastosEntry.COLUMN_NAME_ID+"="+category.getId(), null);

        database.close();
        database = null;

        callback.onSaveDb();
    }

    public synchronized void removeCategoryOnDB(Category category, Callback callback) {
        database = dbSqliteHelper.getWritableDatabase();

        String whereClause = GastosEntry.COLUMN_NAME_ID + "=" + category.getId();

        database.delete(GastosEntry.TABLE_NAME,whereClause, null );

        database.close();
        database = null;

        callback.onSaveDb();
    }

    public interface Callback {
        void onSaveDb();
    }
}
