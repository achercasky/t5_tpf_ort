package com.achercasky.ort.mvp.views;

import com.achercasky.ort.models.Spending;
import com.chachapps.initialclasses.mvp.view.BaseMvpView;

/**
 * Created by achercasky on 09/09/2017.
 */

public interface AddSpendingMvpView extends BaseMvpView{

    void onEmptyCategory();
    void onEmptyAmount();
    void onInvalidAmount();
    void onEmptyDescription();
    void onSuccess(Spending spending);
}
