package com.achercasky.ort.mvp.views;

import com.achercasky.ort.models.Category;
import com.chachapps.initialclasses.mvp.view.BaseMvpView;

/**
 * Created by achercasky on 21/10/2017.
 */

public interface CategoryMvpView extends BaseMvpView {

    void onExistedCategory();
    void onNewCategory(Category category);
    void onEditCategory(Category category);
    void onUsingCategory();
    void onNotDeletedCategory(Category category, int index);
    void onDeleteCategory(Category category, int index);
}
