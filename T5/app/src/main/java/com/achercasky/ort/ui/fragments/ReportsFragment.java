package com.achercasky.ort.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.achercasky.ort.R;
import com.achercasky.ort.db.DbSqliteManager;
import com.achercasky.ort.models.Report;
import com.achercasky.ort.models.Spending;
import com.achercasky.ort.repositories.CategoryRepository;
import com.achercasky.ort.repositories.ReportRepository;
import com.achercasky.ort.utils.DateUtils;
import com.achercasky.ort.utils.Utils;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.twinkle94.monthyearpicker.picker.YearMonthPickerDialog;

import java.math.RoundingMode;
import java.text.DateFormatSymbols;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;

/**
 * Created by achercasky on 23/09/2017.
 */

public class ReportsFragment extends BaseFragment implements YearMonthPickerDialog.OnDateSetListener {

    @BindView(R.id.chart) BarChart barChart;

    @BindView(R.id.reports_month) TextView monthsTxt;
    @BindView(R.id.report_empty_message) TextView emptyTxt;

    @BindView(R.id.relative) RelativeLayout relative;

    @BindView(R.id.report_container_category) LinearLayout containerCategories;

    private List<Report> reportList;

    private ArrayList<String> categoryTitles;

    private int totalF = 0;

    private YearMonthPickerDialog dialog;

    private String fullDate;

    public static ReportsFragment newInstance() {

        Bundle args = new Bundle();

        ReportsFragment fragment = new ReportsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getMainLayoutResId() {
        return R.layout.fragment_reports;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        dialog = new YearMonthPickerDialog(getActivity(), this);

        monthsTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.show();
            }
        });

        fullDate = Utils.capitalizeWord(Utils.getMonthName()) + " " + Utils.getCurrentYear();

        monthsTxt.setText(fullDate);

        fillChart(DateUtils.getYear(),DateUtils.getMonth());
    }

    @Override
    public void onYearMonthSet(int year, int month) {

        totalF = 0;

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);

        SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM yyyy");
        dateFormat.getTimeZone();

        String date = Utils.capitalizeWord(new DateFormatSymbols().getMonths()[month]);
        String yearText = String.valueOf(year);

        fullDate = Utils.capitalizeWord(date) + " " + yearText;

        monthsTxt.setText(fullDate);

        fillChart(yearText, String.valueOf(month +1));

    }

    private void fillChart(String year, String month) {

        reportList = new ArrayList<>();

        for(String cat: DbSqliteManager.getInstance(getActivity()).getDescriptionsFromCategory()) {
            int total = DbSqliteManager.getInstance(getActivity()).getTotalFromCategory(year, month, cat);
            reportList.add(new Report(cat, total));
            totalF = totalF + total;
        }

        if(totalF > 0) {

            ReportRepository.getInstance().setTotal(totalF);
            ReportRepository.getInstance().setReportList(reportList);
            ReportRepository.getInstance().setTitle(fullDate);

            emptyTxt.setVisibility(View.GONE);
            relative.setVisibility(View.VISIBLE);

            categoryTitles = new ArrayList<>();

            List<BarEntry> barEntries = new ArrayList<>();

            for (int i = 0; i < reportList.size(); i++) {

                Float monto = reportList.get(i).getTotal();

                int total = monto.intValue();

                categoryTitles.add(reportList.get(i).getName());

                BarEntry barEntry = new BarEntry(total, i, reportList.get(i).getName());
                barEntries.add(barEntry);
            }

            BarDataSet barDataSet1 = new BarDataSet(barEntries, getString(R.string.categorias));
            barDataSet1.setColors(ColorTemplate.COLORFUL_COLORS);

            BarData data = new BarData(barDataSet1);
            data.setBarWidth(100f);

            barChart.setData(data);
            barChart.setDescription(null);

            barChart.animateY(3000);
            barChart.getXAxis().setGranularity(1f);

            barChart.invalidate();

            for(String title: categoryTitles) {
                final TextView rowTextView = new TextView(getActivity());
                rowTextView.setText(title);
                rowTextView.setPadding(11, 0, 11, 0);

                containerCategories.addView(rowTextView);
            }
        } else {
            emptyTxt.setVisibility(View.VISIBLE);
            relative.setVisibility(View.GONE);
        }
    }
}
