package com.achercasky.ort.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.achercasky.ort.R;
import com.achercasky.ort.db.DbSqliteManager;
import com.achercasky.ort.models.Category;
import com.achercasky.ort.models.Spending;
import com.achercasky.ort.mvp.presenters.AddSpendingPresenter;
import com.achercasky.ort.mvp.views.AddSpendingMvpView;
import com.achercasky.ort.repositories.CategoryRepository;
import com.achercasky.ort.ui.activities.CategoryActivity;
import com.achercasky.ort.utils.DateUtils;
import com.achercasky.ort.utils.SnackbarUtils;

import java.util.List;

import butterknife.BindView;

/**
 * Created by achercasky on 09/09/2017.
 */

public class AddSpendingFragment extends BaseFragment implements PopupMenu.OnMenuItemClickListener, DbSqliteManager.Callback,
        AddSpendingMvpView{

    @BindView(R.id.add_cardview_category) CardView cardViewCategory;

    @BindView(R.id.add_category_edt) EditText editCategory;
    @BindView(R.id.add_monto_edt) EditText editMonto;
    @BindView(R.id.add_descripcion_edt) EditText editDescripcion;

    @BindView(R.id.add_accept_btn) Button accept;
    @BindView(R.id.add_cancel_btn) Button cancel;
    @BindView(R.id.administrar_btn) Button admin;

    @BindView(R.id.add_container_progress) LinearLayout containerProgress;
    @BindView(R.id.add_container) LinearLayout container;

    private AddSpendingPresenter presenter;

    private boolean isComingBack;

    public static AddSpendingFragment newInstance() {

        Bundle args = new Bundle();

        AddSpendingFragment fragment = new AddSpendingFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getMainLayoutResId() {
        return R.layout.fragment_add_spending;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        presenter = new AddSpendingPresenter();
        presenter.attachMvpView(this);

        editCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createPopMenu();
            }
        });

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                containerProgress.setVisibility(View.VISIBLE);

                presenter.validateInputs(editCategory.getText().toString(), editMonto.getText().toString(),
                        editDescripcion.getText().toString());
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        admin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CategoryActivity.startActivity(getActivity());
            }
        });
        
    }

    private void createPopMenu() {

        final List<String> items = DbSqliteManager.getInstance(getActivity()).getDescriptionsFromCategory();
        CategoryRepository.getInstance().saveCategories(items);

        final PopupMenu popupMenu = new PopupMenu(getActivity(), editCategory);

        popupMenu.getMenu().clear();

        for(String item: items) {
            popupMenu.getMenu().add(item);
        }

        popupMenu.show();
        popupMenu.setOnMenuItemClickListener(AddSpendingFragment.this);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {

        editCategory.setText(item.getTitle());

        return true;
    }

    @Override
    public void onSaveDb() {
        containerProgress.setVisibility(View.GONE);
        SnackbarUtils.show(getView(), getString(R.string.spending_saved));
    }

    @Override
    public void onEmptyCategory() {
        containerProgress.setVisibility(View.GONE);
        SnackbarUtils.show(getView(), getString(R.string.spending_empty_category));
    }

    @Override
    public void onEmptyAmount() {
        containerProgress.setVisibility(View.GONE);
        SnackbarUtils.show(getView(), getString(R.string.spending_empty_amount));
    }

    @Override
    public void onInvalidAmount() {
        containerProgress.setVisibility(View.GONE);
        SnackbarUtils.show(getView(), getString(R.string.spending_invalid_amount));
    }

    @Override
    public void onEmptyDescription() {
        containerProgress.setVisibility(View.GONE);
        SnackbarUtils.show(getView(), getString(R.string.spending_empty_description));
    }

    @Override
    public void onSuccess(Spending spending) {
        spending.setFecha(DateUtils.getDateTime());
        DbSqliteManager.getInstance(getActivity()).writeOnDB(spending, AddSpendingFragment.this);
    }

    @Override
    public Context getMvpContext() {
        return getActivity();
    }

    @Override
    public void onError(Throwable throwable) {

    }

    @Override
    public void onNoInternetConnection() {

    }

    @Override
    public void onErrorCode(String s) {

    }
}
